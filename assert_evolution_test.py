from operator import eq, gt

from pytest import raises
from pytest import mark

from test_helpers import assert_evolution_class   as cla
from test_helpers import assert_evolution_closure as clo
from test_helpers import assert_evolution_gen     as gen


parameters = ('assert_evolution', (cla, clo, gen))

@mark.parametrize(*parameters)
def test_assert_evolution_should_work_with_constant_expression_and_eq(assert_evolution):
    assert_invariant = assert_evolution(lambda:x, eq)
    for x in 'aaaaa':
        assert_invariant()

@mark.parametrize(*parameters)
def test_assert_evolution_should_fail_with_non_constant_expression_and_eq(assert_evolution):
    assert_invariant = assert_evolution(lambda:x, eq)
    x = 1
    assert_invariant()
    assert_invariant()
    x = 2
    with raises(AssertionError):
        assert_invariant()

@mark.parametrize(*parameters)
def test_assert_evolution_should_work_with_increasing_expression_and_gt(assert_evolution):
    assert_increasing = assert_evolution(lambda:x, gt)
    for x in range(20):
        assert_increasing()

    x -= 2
    with raises(AssertionError):
        assert_increasing()

@mark.parametrize(*parameters)
def test_assert_evolution_should_fail_with_constant_expression_and_gt(assert_evolution):
    assert_increasing = assert_evolution(lambda:x, gt)
    x = 1
    assert_increasing()
    with raises(AssertionError):
        assert_increasing()
