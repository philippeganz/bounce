from sys import argv

from Particle import Particle
from Colour   import Colour as C

if 'pyg' in argv: from PygletDisplay  import PygletDisplay  as Display
else            : from TkinterDisplay import TkinterDisplay as Display



d = Display((Particle(30, (200,200), ( 80, 150), C.RED),
             Particle(40, (200,200), (150,  80), C.GREEN),
             Particle(35, (200,200), ( 90, 140), C.CYAN)))
d.go()
