from Vector import Vector2D as Vector

class Particle:

    def __init__(self, r, p, v, c):
        self.r = r
        self.p = Vector(*p)
        self.v = Vector(*v)
        self.c = c

    @property
    def x(self):
        return self.p.x

    @property
    def y(self):
        return self.p.y

    @x.setter
    def x(self, value):
        self.p.x = value

    @y.setter
    def y(self, value):
        self.p.y = value

    @property
    def vx(self):
        return self.v.x

    @property
    def vy(self):
        return self.v.y

    @vx.setter
    def vx(self, value):
        self.v.x = value

    @vy.setter
    def vy(self, value):
        self.v.y = value

    def move(self, dt):
        self.x += self.vx * dt
        self.y += self.vy * dt

    def bounce(self, box):
        xmin, xmax, ymin, ymax = box

        excess = xmin - (self.x - self.r)
        if excess > 0:
            self. x += 2 * excess
            self.vx = - self.vx

        excess = (self.x + self.r) - xmax
        if excess > 0:
            self. x -= 2 * excess
            self.vx = - self.vx

        excess = ymin - (self.y - self.r)
        if excess > 0:
            self. y += 2 * excess
            self.vy = - self.vy

        excess = (self.y + self.r) - ymax
        if excess > 0:
            self. y -= 2 * excess
            self.vy = - self.vy
