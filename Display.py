from abc import ABC, abstractmethod

class Display(ABC):

    def __init__(self, ps):
        self.ps = ps

    def update(self, dt):
        for p in self.ps:
            p.move(dt)
            p.bounce(self.bounding_box)

    @abstractmethod
    def go(self): pass

    @property
    @abstractmethod
    def bounding_box(self): pass

    @abstractmethod
    def draw_circle(self, x,y, r, c): pass

    # I real life we would need many more such methods for drawing
    # primitive shapes, so that the client can compose arbitrary
    # pictures. We'll leave these out here, as there is nothing
    # especially interesting about them, once we have understood the
    # principle.

    # @abstractmethod
    # def draw_line(self, x0,y0, x1,y1): pass
